# Menu Condition

This provides a condition based on menu position. For example,
you can use it to specify that a block should only show for a
particular menu item and all its children.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/menu_condition).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/menu_condition).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

- Joachim Noreiko - [joachim](https://www.drupal.org/u/joachim)
- Rupert Jabelman - [rupertj](https://www.drupal.org/u/rupertj)
- Neslee Canil Pinto - [Neslee Canil Pinto](https://www.drupal.org/u/neslee-canil-pinto)